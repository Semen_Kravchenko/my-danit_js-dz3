//Теория:

// 1 вопрос:
//Функции в программировании нужны для того, чтобы не повторяться, т.е, если у нас есть какой-нибудь кусок кода, который будет несколько раз повторяться на странице, то мы можем его записать в функцию, чтобы в любой момент к нему обратиться. Также, функция позволяет структурировать код: если необходимо реализовать какое-то действие, состоящее из множества действий, то эти множества можно записать в несколько небольших функции, из которых мы будем создавать одну большую.(сорри за тавтологию...)

// 2 вопрос:
//Для хранения информации. Фактически, когда мы передаем аргумент, то создаем внутри функции локальную переменную. Во время вызова функции, если мы передадим аргументу определенное значение, то оно скопируется и будет в нём храниться.

//Задание:

let firstNum;
let secondNum;
let mathOperation;
let check;

do {
    firstNum = prompt(`Введите первое число: `, firstNum);
    secondNum = prompt(`Введите второе число: `, secondNum);
    mathOperation = prompt(`Введите математическую операцию: сложение -> (+), вычитание -> (-), умножение -> (*), деление -> (/)`);
    check = isNaN(firstNum) || firstNum === null || firstNum === "" || isNaN(secondNum) || secondNum === null || secondNum === "";
}
while(check);

function calcResult(getFirstNum, getSecondNum, getOperation) {
    getFirstNum = +firstNum;
    getSecondNum = +secondNum;
    getOperation = mathOperation;
    switch (getOperation) {
        case "+":
            return getFirstNum + getSecondNum;
        case "-":
            return getFirstNum - getSecondNum;
        case "*":
            return getFirstNum * getSecondNum;
        case "/":
            if (getSecondNum !== 0) {
                return getFirstNum / getSecondNum;
            }
            else {
                return `Недопустимая операция!`;
            }
        default:
            return `Такой операции нет!`;
    }
}
console.log(`Результат: ${calcResult()}`);
